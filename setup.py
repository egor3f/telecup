from setuptools import setup

setup(
    name='telecup',
    version='1.0',
    py_modules=['telecup'],
    install_requires=[
        'Click',
        'telethon'
    ],
    entry_points={
        'console_scripts': 'telecup=telecup_cli:cli'
    }
)
